# Overview

This is an easy to stand up implementation of [the pi hole project](https://pi-hole.net/).
They have a great project over there, and deserve some contributions if you can
afford to donate.

# Set up

Easy set up instruction below:

1. ```git clone https://gitlab.com/radiomime/hole.git```
    * This will clone this project onto your raspberry pi.
2. ```cp .sample.env .env```
    * Copy an example environment. 
3. ```vi .env```
    * Change the password to the password you'll use to log into the pi-hole
    interface. Make it unique and strong
4. ```docker-compose up -d```
    * This will start pi hole. It will also restart the project when your
    raspberry pi reboots.